import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    this.setArrow();
  }
  setArrow() {
    if (document.querySelector(".custom-pagination .pagination .first a")) {
      document.querySelector(
        ".custom-pagination .pagination .first a"
      ).innerHTML = '<i class="fa-solid fa-angles-left"></i>';
    }
    if (document.querySelector(".custom-pagination .pagination .previous a")) {
      document.querySelector(
        ".custom-pagination .pagination .previous a"
      ).innerHTML = '<i class="fa-solid fa-angle-left"></i>';
    }
    if (document.querySelector(".custom-pagination .pagination .next a")) {
      document.querySelector(
        ".custom-pagination .pagination .next a"
      ).innerHTML = '<i class="fa-solid fa-angle-right"></i>';
    }
    if (document.querySelector(".custom-pagination .pagination .last a")) {
      document.querySelector(
        ".custom-pagination .pagination .last a"
      ).innerHTML = '<i class="fa-solid fa-angles-right"></i>';
    }

    console.log("Ok");
  }
}
