<?php

namespace App\DataFixtures;

use App\Entity\CarBrand;
use App\Entity\CarOption;
use App\Entity\CarType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class BrandFixtures extends Fixture implements FixtureGroupInterface
{

    public static function getGroups(): array
    {
        return ['other'];
    }

    const CAR_BRAND = array(
        "Alfa Romeo",
        "Audi",
        "BMW",
        "Citroën",
        "DACIA",
        "Fiat",
        "Ford",
        "Mercedes-Benz",
        "Nissan",
        "Opel",
        "Peugeot",
        "Renault",
        "SEAT",
        "Toyota",
        "Volkswagen",
        "Abarth",
        "Alpine",
        "Aston Martin",
        "Bentley",
        "Bugatti",
        "Cadillac",
        "Chevrolet",
        "Chrysler",
        "Corvette",
        "Cupra",
        "Daewoo",
        "Daihatsu",
        "Daimler",
        "Dodge",
        "Ferrari",
        "Honda",
        "Hummer",
        "Hyundai",
        "Infiniti",
        "Iveco",
        "Jaguar",
        "Jeep",
        "KIA",
        "Lamborghini",
        "Lancia",
        "Land Rover",
        "Lexus",
        "Lotus",
        "MAN",
        "Maserati",
        "Mazda",
        "MEGA",
        "MG",
        "MINI",
        "Mitsubishi",
        "Pontiac",
        "Porsche",
        "Renault Trucks",
        "Rolls-Royce",
        "Rover",
        "SAAB",
        "Skoda",
        "Ssangyong",
        "Subaru",
        "Suzuki",
        "Tesla",
        "Volvo",
    );

    const CAR_OPTIONS = array(
        'ABS',
        'ESP',
        'climatisation',
        'Centralisation',
        'Vitres électriques avant',
        'Vitres électriques arriere',
        'Roue de secours',
        'Kit de réparation de roue',
        'Antibrouillards',
        'Direction assistée',
        'Park Assist',
        'Caméra de recul',
        'Android Auto',
        'Apple Car Play',
    );

    const CAR_TYPE = array(
        'SUV',
        'Citadine',
        'Berline',
        '4x4'
    );

    public function load(ObjectManager $manager): void
    {
        foreach (self::CAR_BRAND as $key => $brand) {
            $newBrand = new CarBrand();
            $newBrand->setBrand($brand);

            $manager->persist($newBrand);
        }

        foreach (self::CAR_OPTIONS as $key => $option) {
            $newOption = new CarOption();
            $newOption->setOptionName($option);

            $manager->persist($newOption);
        }

        foreach (self::CAR_TYPE as $key => $type) {
            $newType = new CarType();
            $newType->setType($type);

            $manager->persist($newType);
        }

        $manager->flush();
    }
}
