<?php

namespace App\Controller\Admin;

use App\Repository\CarBrandRepository;
use App\Repository\CarRepository;
use Symfony\UX\Chartjs\Model\Chart;
use App\Repository\CarTypeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminStatsController extends AbstractController
{

    private const COLORS = array(
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
        '#845EC2',
        '#FF6F91',
        '#D65DB1',
        '#FFC75F',
        '#FF9671',
        '#9BDE7E',
        '#F9F871',
        '#039590',
        '#4BBC8E',
        '#2F4858',
        '#1C6E7D',
    );

    public function __construct(
        private ChartBuilderInterface $chartBuilderInterface,
        private CarRepository $carRepository,
        private CarTypeRepository $carTypeRepository,
        private CarBrandRepository $carBrandRepository
    ) {
    }
    #[Route('/admin/stats', name: 'app_admin_stats')]
    public function index(): Response
    {

        $carType = $this->carTypeRepository->findAll();

        $carTypeList = [];
        foreach ($carType as $key => $type) {
            $carTypeList[$key] = $type->getType();
        }

        $chart = $this->chartBuilderInterface->createChart(Chart::TYPE_BAR);

        $carTypeCount = array();
        foreach ($carType as $key => $type) {
            $carTypeCount[$key] = count($this->carRepository->findBy(['Type' => $type]));
        }






        $chart = $this->chartBuilderInterface->createChart(Chart::TYPE_DOUGHNUT);

        $chart->setData([
            'labels' => $carTypeList,
            'datasets' => [
                [
                    'label' => 'Nombre de voitures listées',
                    'backgroundColor' => self::COLORS,
                    'data' => $carTypeCount,
                ],
            ],
        ]);


        $chart2 = $this->chartBuilderInterface->createChart(Chart::TYPE_DOUGHNUT);

        $chart2->setData([
            'labels' => ['Publiées', 'En attente'],
            'datasets' => [
                [
                    'label' => 'Nombre de voitures listées',
                    'backgroundColor' => self::COLORS,
                    'data' => [count($this->carRepository->findBy(['isPublished' => true])), count($this->carRepository->findBy(['isPublished' => false]))],
                ],
            ],
        ]);


        $chart3 = $this->chartBuilderInterface->createChart(Chart::TYPE_BAR);

        $allBrand = $this->carBrandRepository->findAll();
        $labels = array();

        foreach ($allBrand as $key => $value) {
            $labels[$key] = $value->getBrand();
            // dump($value);
        }

        $datasetChart3 = array();

        dump(count($allBrand));

        if (count($allBrand) < 1) {
            $datasetChart3['data'] = null;
            $datasetChart3['backgroundColor'] = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $datasetChart3['label'] = null;
        } else {
            foreach ($allBrand as $key => $brand) {
                $datasetChart3['data'][$key] = count($this->carRepository->findBy(['Brand' => $brand]));
                $datasetChart3['backgroundColor'][$key] = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
                $datasetChart3['label'][$key] = $brand->getBrand();

                // array_push($datasetChart3[$key]['data'], [count($this->carRepository->findBy(['Brand' => $brand]))]);
            }
        }

        // dump($datasetChart3, $labels);
        // dump($labels);
        // dump($datasetChart3);

        $chart3->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => $datasetChart3['label'],
                    'backgroundColor' => $datasetChart3['backgroundColor'],
                    'data' => $datasetChart3['data'],
                ],
            ],
        ]);


        // $chart3 = $this->chartBuilderInterface->createChart(Chart::TYPE_LINE);

        // $chart3->setData([
        //     'labels' => ['Publiées', 'En attente'],
        //     'datasets' => [
        //         [
        //             'label' => 'Nombre de voitures listées',
        //             'backgroundColor' => self::COLORS,
        //             'data' => [count($this->carRepository->findBy(['isPublished' => true])), count($this->carRepository->findBy(['isPublished' => false]))],
        //         ],
        //     ],
        // ]);
        dump($chart, $chart2, $chart3);

        return $this->render('admin_stats/index.html.twig', [
            'controller_name' => 'AdminStatsController',
            'chart' => [$chart, $chart2, $chart3],
        ]);
    }
}
