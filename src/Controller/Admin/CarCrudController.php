<?php

namespace App\Controller\Admin;

use App\Entity\Car;
use DateTimeImmutable;
use App\Entity\CarType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use phpDocumentor\Reflection\Types\Boolean;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Car::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->hideOnForm(),
            FormField::addTab('Vehicule'),
            AssociationField::new('Brand', 'Marque'),
            AssociationField::new('Type', 'Categorie'),
            TextField::new('Model', 'Modele'),
            TextField::new('Plate', 'Immatriculation'),
            ChoiceField::new('FuelType', 'Type')->autocomplete()
                ->setChoices([
                    'Hybrid' => 'Hybrid',
                    'Diesel' => 'Diesel',
                    'Electric' => 'Electric',
                    'Gas' => 'Gas',
                ]),
            ChoiceField::new('GearBoxType', 'Type de boite')->autocomplete()
                ->setChoices([
                    'AUTOMATIC' => 'AUTOMATIC',
                    'MANUAL' => 'MANUAL',
                ]),
            DateField::new('PMC', 'Premiere mise en circulation'),
            BooleanField::new('isPublished', 'Publier la voiture')->renderAsSwitch(false),

            FormField::addTab('Informations supplémentaires'),
            ImageField::new('Picture', 'Photo')
                ->setUploadDir('public/' . $this->getParameter('vich_path.car_main_image'))
                ->setUploadedFileNamePattern('[ulid].[extension]')
                ->setBasePath($this->getParameter('vich_path.car_main_image')),
            // ->setBasePath('uploads/avatars')
            // ->setUploadDir('public/avatars/uploads')
            // ->setUploadedFileNamePattern('[ulid].[extension]'),
            TextField::new('Color', 'Couleur'),
            IntegerField::new('Door', 'Portes'),
            AssociationField::new('Options', 'Options'),


            DateField::new('CreatedAt', 'Publication')->hideOnForm(),
            DateField::new('UpdatedAt', 'Mise à jour')->hideOnForm(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }


    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {

        $entityInstance->setCreatedAt(new DateTimeImmutable());
        $entityInstance->setUpdatedAt(new DateTimeImmutable());


        parent::persistEntity($em, $entityInstance);
    }
}
