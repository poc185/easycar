<?php

namespace App\Controller;

use App\Repository\CarRepository;
use App\Repository\CarTypeRepository;
use App\Repository\CarBrandRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class HomeController extends AbstractController
{

    public function __construct(
        private CarRepository $carRepository,
        private PaginatorInterface $paginatorInterface,
        private CarBrandRepository $carBrandRepository,
        private CarTypeRepository $carTypeRepo,
        private NormalizerInterface $normalizerInterface
    ) {
    }
    #[Route('/', name: 'app_home')]
    public function index(Request $request): Response
    {



        $pagination = $this->paginatorInterface->paginate(
            $this->carRepository->findBy(['isPublished' => true]),
            $request->query->getInt('page', 1),
            6,
            []
        );


        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'car_list' => $this->carRepository->findBy(['isPublished' => true]),
            'pagination' => $pagination
        ]);
    }

    #[Route('/latest', name: 'app_latest')]
    public function latestCar(): Response
    {

        dump($this->carRepository->findBy(['isPublished' => true], ["CreatedAt" => 'DESC']));

        return $this->render('home/latest.html.twig', [
            'controller_name' => 'HomeController',
            'car_list' => $this->carRepository->findBy(['isPublished' => true], ["CreatedAt" => 'DESC'])
        ]);
    }

    #[Route('/updated', name: 'app_updated')]
    public function lastUpdated(): Response
    {


        return $this->render('home/updated.html.twig', [
            'controller_name' => 'HomeController',
            'car_list' => $this->carRepository->findBy(['isPublished' => true], ["UpdatedAt" => 'DESC']),
            'today' => new \DateTimeImmutable(),
        ]);
    }

    #[Route('/brands', name: 'app_brands')]
    public function allbrands(Request $request): Response
    {

        $pagination = $this->paginatorInterface->paginate(
            $this->normalizerInterface->normalize($this->carBrandRepository->findBy([], ['Brand' => 'ASC']), null, ['groups' => ['Brand:collection:read']]),
            $request->query->getInt('page', 1),
            16,
            []
        );

        // dd($this->normalizerInterface->normalize($this->carBrandRepository->findAll(), null, ['groups' => ['Brand:collection:read']]));
        dump($this->normalizerInterface->normalize($this->carBrandRepository->findAll(), null, ['groups' => ['Brand:collection:read']]));

        return $this->render('home/brands.html.twig', [
            'controller_name' => 'HomeController',
            'today' => new \DateTimeImmutable(),
            'pagination' => $pagination
        ]);
    }
}
