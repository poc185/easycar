<?php

# src/EventSubscriber/EasyAdminSubscriber.php

namespace App\EventSubscriber;

use App\Entity\Car;
use DateTimeImmutable;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class CarSubscriber implements EventSubscriberInterface
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityUpdatedEvent::class => ['updateEntity'],
        ];
    }

    public function updateEntity(BeforeEntityUpdatedEvent $event)
    {
        $entity = $event->getEntityInstance();

        // dump($entity, 'BORDEL');

        // if (!($entity instanceof Car)) {
        //     return;
        // }

        $entity->setUpdatedAt(new \DateTimeImmutable());
        // $this->setUpdatedDateEntity($entity);
    }

    private function setUpdatedDateEntity(Car $car)
    {
        $car->setUpdatedAt(new \DateTimeImmutable());
    }
}
